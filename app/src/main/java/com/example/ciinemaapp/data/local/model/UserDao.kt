package com.example.ciinemaapp.data.local.model

import androidx.room.*

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addUser(user: User)

    @Query("select * from User where userName=:username and password=:password")
    fun getUserLogin(username: String, password: String): User

    @Query("select * from User where userId=:userId")
    fun getUserById(userId: Int): User

    @Update
    fun updateUser(user: User)
}