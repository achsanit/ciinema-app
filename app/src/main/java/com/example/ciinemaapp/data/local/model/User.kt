package com.example.ciinemaapp.data.local.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class User(
    @PrimaryKey(autoGenerate = true)
    val userId: Int,
    val userName: String,
    val email: String,
    val password: String,
    val imageProfile: String?
): Parcelable
