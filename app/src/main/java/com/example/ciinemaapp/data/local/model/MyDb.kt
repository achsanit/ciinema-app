package com.example.ciinemaapp.data.local.model

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [User::class], version = 2, exportSchema = false)
abstract class MyDb: RoomDatabase() {

    abstract fun userDao(): UserDao

}