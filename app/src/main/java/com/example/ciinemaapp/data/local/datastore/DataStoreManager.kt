package com.example.ciinemaapp.data.local.datastore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class DataStoreManager(private val context: Context) {

    suspend fun setLoginStatus() {
        context.dataStore.edit { pref ->
            pref[LOGIN_KEY] = true
        }
    }

    suspend fun setLogoutStatus() {
        context.dataStore.edit { pref ->
            pref[LOGIN_KEY] = false
        }
    }

    suspend fun setUserId(userId: Int) {
        context.dataStore.edit { pref ->
            pref[USERID_KEY] = userId
        }
    }

    fun getUserId() : Flow<Int> {
        return context.dataStore.data.map { pref ->
            pref[USERID_KEY] ?: 0
        }
    }

    fun statusLogin() : Flow<Boolean> {
        return context.dataStore.data.map { pref ->
            pref[LOGIN_KEY] ?: false
        }
    }

    companion object {
        private const val DATASTORE_NAME = "data_store"

        private val LOGIN_KEY = booleanPreferencesKey("login_key")
        private val USERID_KEY = intPreferencesKey("user_id")

        private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = DATASTORE_NAME)
    }
}