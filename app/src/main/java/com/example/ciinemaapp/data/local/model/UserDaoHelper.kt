package com.example.ciinemaapp.data.local.model

class UserDaoHelper(val userDao: UserDao) {
    fun addUser(user: User) {
        userDao.addUser(user)
    }

    fun getUserLogin(username: String, password: String): User {
        return userDao.getUserLogin(username, password)
    }

    fun getUserById(userId: Int): User {
        return userDao.getUserById(userId)
    }

    fun updateUser(user: User) {
        userDao.updateUser(user)
    }
}