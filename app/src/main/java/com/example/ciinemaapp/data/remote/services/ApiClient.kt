package com.example.ciinemaapp.data.remote.services

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {
    //base url
    const val BASE_URL = "https://api.themoviedb.org/"
//    val base_url = "https://api.themoviedb.org/3/movie"

    //logging
    private val logging: HttpLoggingInterceptor
        get() {
            val logging = HttpLoggingInterceptor()

            return logging.apply {
                logging.level = HttpLoggingInterceptor.Level.BODY
            }
        }

    private val client = OkHttpClient.Builder().addInterceptor(logging).build()

    //create instance
    val instance: ApiService by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        retrofit.create(ApiService::class.java)
    }
}