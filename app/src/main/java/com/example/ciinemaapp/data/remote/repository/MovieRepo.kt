package com.example.ciinemaapp.data.remote.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.ciinemaapp.BuildConfig
import com.example.ciinemaapp.data.remote.model.DetailMovieResponse
import com.example.ciinemaapp.data.remote.model.Genre
import com.example.ciinemaapp.data.remote.model.MovieResponse
import com.example.ciinemaapp.data.remote.model.Result
import com.example.ciinemaapp.data.remote.services.ApiClient
import com.example.ciinemaapp.data.remote.services.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class MovieRepo @Inject constructor(private val apiHelper: ApiHelper) {

    fun getNowPlaying(listMovie: MutableLiveData<List<Result>>) {
        apiHelper.getNowPlaying(
            apiKey = BuildConfig.API_KEY,
            page = 1,
            language = "en-US"
        ).enqueue(object : Callback<MovieResponse>{
            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                val result = response.body()?.results
                val code = response.code()
                if (code == 200) {
                    listMovie.postValue(result)
                }
            }
            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                listMovie.postValue(null)
                Log.d("onFailure", "$t")
            }

        })
    }

    fun getSimilarMovie(
        movieId: Int,
        listMovie: MutableLiveData<List<Result>>
    ) {
        apiHelper.getSimilarMovie(
            movieId = movieId,
            apiKey = BuildConfig.API_KEY,
            language = "en-US"
        ).enqueue(object : Callback<MovieResponse> {
            override fun onResponse(
                call: Call<MovieResponse>,
                response: Response<MovieResponse>
            ) {
                val code = response.code()
                if (code == 200 ) {
                    listMovie.postValue(response.body()?.results)
                }
            }
            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                Log.d("onFailure", "$t")
            }
        })
    }

    fun getDetailMovie(
        movieId: Int,
        detailMovie: MutableLiveData<DetailMovieResponse>,
        genreMovie: MutableLiveData<List<Genre>>
    ) {
        apiHelper.getDetailMovie(
            movieId = movieId,
            apiKey = BuildConfig.API_KEY,
            language = "en-US"
        ) .enqueue(object : Callback<DetailMovieResponse> {
            override fun onResponse(
                call: Call<DetailMovieResponse>,
                response: Response<DetailMovieResponse>
            ) {
                val code = response.code()
                if (code == 200 ) {
                    detailMovie.postValue(response.body())
                    genreMovie.postValue(response.body()?.genres)
                }
            }

            override fun onFailure(call: Call<DetailMovieResponse>, t: Throwable) {
                Log.d("onFailure", "$t")
            }

        })
    }

    fun getPopularMovie(listMovie : MutableLiveData<List<Result>>) {
        apiHelper.getPopularMovie(
            apiKey = BuildConfig.API_KEY,
            page = 1,
            language = "en-US"
        ).enqueue(object : Callback<MovieResponse> {
            override fun onResponse(
                call: Call<MovieResponse>,
                response: Response<MovieResponse>
            ) {
                val code = response.code()
                val body = response.body()?.results
                if (code == 200) {
                    listMovie.postValue(body!!)
                }

            }
            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                Log.d("onFailure", "$t")
            }

        })
    }

    fun getTopRatedMovie(listMovie: MutableLiveData<List<Result>>) {
        apiHelper.getTopRatedMovie(
            apiKey = BuildConfig.API_KEY,
            page = 1,
            language = "en-US"
        ).enqueue(object : Callback<MovieResponse> {
            override fun onResponse(
                call: Call<MovieResponse>,
                response: Response<MovieResponse>
            ) {
                val code = response.code()
                val body = response.body()?.results
                if (code == 200) {
                    listMovie.postValue(body!!)
                }

            }
            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                Log.d("onFailure", "$t")
            }

        })
    }

}