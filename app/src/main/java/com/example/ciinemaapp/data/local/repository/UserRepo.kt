package com.example.ciinemaapp.data.local.repository

import androidx.lifecycle.MutableLiveData
import com.example.ciinemaapp.data.local.model.User
import com.example.ciinemaapp.data.local.model.UserDao
import com.example.ciinemaapp.data.local.model.UserDaoHelper
import javax.inject.Inject

class UserRepo @Inject constructor(private val userDaoHelper: UserDaoHelper) {

    fun getUserById(userId: Int, getUserById: MutableLiveData<User>) {
        val getUser = userDaoHelper.getUserById(userId)
        getUserById.postValue(getUser)
    }

    fun getUserLogin(userName: String, password: String, user: MutableLiveData<User>){
        val checkUser = userDaoHelper.getUserLogin(userName,password)
        user.postValue(checkUser)
    }

    fun updateUser(user: User, getUserById: MutableLiveData<User>){
        userDaoHelper.updateUser(user)
        getUserById(user.userId,getUserById)
    }

    fun addUser(user: User) {
        userDaoHelper.addUser(user)
    }

}