package com.example.ciinemaapp.data.remote.services

import com.example.ciinemaapp.data.remote.model.DetailMovieResponse
import com.example.ciinemaapp.data.remote.model.MovieResponse
import retrofit2.Call

class ApiHelper(private val apiService: ApiService) {
    fun getPopularMovie(
        apiKey: String,
        page: Int,
        language: String
    ): Call<MovieResponse> {
        return apiService.getPopularMovie(apiKey,page, language)
    }

    fun getTopRatedMovie(
        apiKey: String,
        page: Int,
        language: String
    ): Call<MovieResponse> {
        return apiService.getTopRatedMovie(apiKey,page, language)
    }

    fun getDetailMovie(
        movieId: Int,
        apiKey: String,
        language: String
    ): Call<DetailMovieResponse> {
        return apiService.getDetailMovie(movieId, apiKey, language)
    }

    fun getSimilarMovie(
        movieId: Int,
        apiKey: String,
        language: String
    ): Call<MovieResponse> {
        return apiService.getSimilarMovie(movieId, apiKey, language)
    }

    fun getNowPlaying(
        apiKey: String,
        page: Int,
        language: String
    ): Call<MovieResponse> {
        return apiService.getNowPlaying(apiKey, page, language)
    }
}