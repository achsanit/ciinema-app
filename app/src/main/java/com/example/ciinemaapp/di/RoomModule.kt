package com.example.ciinemaapp.di

import android.app.Application
import androidx.room.Room
import com.example.ciinemaapp.data.local.datastore.DataStoreManager
import com.example.ciinemaapp.data.local.model.MyDb
import com.example.ciinemaapp.data.local.model.UserDao
import com.example.ciinemaapp.data.local.model.UserDaoHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {
    @Provides
    @Singleton
    fun provideDataStoreManager(application: Application) : DataStoreManager {
        return DataStoreManager(application)
    }

    @Provides
    @Singleton
    fun provideDB(application: Application) : MyDb {
        return Room.databaseBuilder(application,MyDb::class.java,"my_db").build()
    }

    @Singleton
    @Provides
    fun provideUserDao(myDb: MyDb) : UserDao {
        return myDb.userDao()
    }

    @Singleton
    @Provides
    fun provideUserDaoHelper(userDao: UserDao) : UserDaoHelper {
        return UserDaoHelper(userDao)
    }
}