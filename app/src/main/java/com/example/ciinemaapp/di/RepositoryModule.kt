package com.example.ciinemaapp.di

import com.example.ciinemaapp.data.local.model.UserDaoHelper
import com.example.ciinemaapp.data.local.repository.UserRepo
import com.example.ciinemaapp.data.remote.repository.MovieRepo
import com.example.ciinemaapp.data.remote.services.ApiHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object RepositoryModule {
    @ViewModelScoped
    @Provides
    fun provideMovieRepo(apiHelper: ApiHelper): MovieRepo {
        return MovieRepo(apiHelper)
    }

    @ViewModelScoped
    @Provides
    fun provideUserRepo(userDaoHelper: UserDaoHelper): UserRepo {
        return UserRepo(userDaoHelper)
    }
}