package com.example.ciinemaapp.ui.view.compose.component

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.rememberLottieComposition
import com.example.ciinemaapp.R

@Preview
@Composable
fun LogoImage() {
    Column (
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(painter = painterResource(id = R.drawable.logo_ciinema), contentDescription = "logo image")
    }
}

@Preview
@Composable
fun LottieLoad() {
    val composition by rememberLottieComposition(spec = LottieCompositionSpec.RawRes(R.raw.ciinema_logo))
    LottieAnimation(composition = composition)
}

@Composable
fun ImageProfile(onClick: ()->Unit) {
    Box(Modifier.clickable { onClick() }) {
        Image(
            painter = painterResource(id = R.drawable.ic_launcher_background),
            contentDescription = "profile",
            modifier = Modifier
                .size(128.dp)
                .clip(shape = RoundedCornerShape(8.dp)),
        )
        Image(
            painter = painterResource(id = R.drawable.ic_baseline_camera_alt_24),
            contentDescription = "camera image",
            modifier = Modifier
                .align(Alignment.BottomEnd)
                .padding(8.dp)
        )
    }
}