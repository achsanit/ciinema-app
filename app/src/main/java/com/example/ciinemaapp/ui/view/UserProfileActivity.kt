package com.example.ciinemaapp.ui.view

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.material.AlertDialog
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.example.ciinemaapp.data.local.model.User
import com.example.ciinemaapp.R
import com.example.ciinemaapp.ui.view.compose.component.ButtonPrimary
import com.example.ciinemaapp.ui.view.compose.component.ButtonPrimaryCompose
import com.example.ciinemaapp.ui.view.compose.component.ButtonSecondary
import com.example.ciinemaapp.ui.view.compose.component.ImageProfile
import com.example.ciinemaapp.ui.view.compose.normalText
import com.example.ciinemaapp.ui.view.compose.passwordTextFieldPrimary
import com.example.ciinemaapp.ui.view.compose.textFieldPrimary
import com.example.ciinemaapp.ui.viewmodel.LoginViewModel
import com.example.ciinemaapp.ui.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserProfileActivity : AppCompatActivity() {
    private val viewModel: UserViewModel by viewModels()
    private val viewModelLogin: LoginViewModel by viewModels()
    private lateinit var imageProfile: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Column(modifier = Modifier
                .background(color = colorResource(id = R.color.dark_700))
                .fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                val userId by viewModel.getUserIdDS().observeAsState(0)
                viewModel.getUserById(userId)
                val user: State<User> = viewModel.getUser.observeAsState(User(0,"User","email","123",null))
                val _username = user.value.userName
//                var username by remember { TextFieldValue(_username) }
                var email by remember { mutableStateOf(TextFieldValue(user.value.email)) }

                Row(modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp, vertical = 4.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.back_ic),
                        contentDescription = "back",
                        modifier = Modifier.clickable { finish() }
                    )
                    normalText(
                        text = "User Profile: ${user.value.userName}",
                        style = TextStyle(
                            fontSize = 24.sp,
                            fontWeight = FontWeight.SemiBold,
                            color = colorResource(id = R.color.white)
                        )
                    )
                    Image(
                        painter = painterResource(id = R.drawable.back_ic),
                        contentDescription = "back",
                        modifier = Modifier.alpha(0f)
                    )
                }

                Spacer(modifier = Modifier.height(36.dp))
                ImageProfile {
                    checkingPermission()
                }

                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(64.dp, 64.dp, 64.dp, 52.dp)
                ) {
                    textFieldPrimary(
                        label = "Username",
                        textFieldValue = TextFieldValue(_username),
                        onValueChange = { }
                    )

                    textFieldPrimary(
                        label = "Email",
                        textFieldValue = email,
                        onValueChange = { email = it }
                    )

                    passwordTextFieldPrimary(
                        label = "Old Password",
                        textFieldValue = TextFieldValue(""),
                        onValueChange = {  }
                    )

                    passwordTextFieldPrimary(
                        label = "New Password",
                        textFieldValue = TextFieldValue(""),
                        onValueChange = {  }
                    )

                }

                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 64.dp)
                ) {
                    ButtonPrimary(text = "SAVE CHANGES") {

                    }
                    ButtonSecondary(text = "LOGOUT") {
                        logout()
                    }
                }
            }
        }

//        viewModel.getUserIdDS().observe(this, Observer {
//            getUSer(it)
//        })

//        binding.btnEdit.setOnClickListener { updateUser() }
//
//        binding.btnLogout.setOnClickListener { logout() }
//
//        binding.btnBack.setOnClickListener { finish() }
//
//        binding.ivImageProfile.setOnClickListener { checkingPermission() }
    }

//    private fun updateUser(){
//        val newUsername = binding.etUsername.editText?.text.toString()
//        val newEmail = binding.etEmail.editText?.text.toString()
//        val oldPass = binding.etOldPassword.editText?.text.toString()
//        val newPass = binding.etNewPassword.editText?.text.toString()
//        val pass = viewModel.getUser.value?.password
//        val userId = viewModel.getUser.value?.userId
//
//        if (inputCheck(newUsername,newEmail, oldPass, newPass)) {
//
//            if (oldPass == pass) {
//                updateUserRoom(userId!!,newUsername,newEmail,newPass)
//            } else {
//                Toast.makeText(this, "Old password is wrong!!", Toast.LENGTH_SHORT).show()
//            }
//
//        } else {
//            Log.d("pass", "$pass")
//            Toast.makeText(this,"Please complete the field", Toast.LENGTH_SHORT).show()
//        }
//    }
//
//    private fun updateUserRoom(userId: Int,username: String,email: String,pass:String) {
//        val updatedUser = User(userId, username, email, pass, imageProfile)
//        viewModel.updateUser(updatedUser)
//        getUSer(userId)
//
//        Toast.makeText(this, "Updated successful...", Toast.LENGTH_SHORT).show()
//    }
//
//    private fun getUSer(userId:Int) {
//        if (userId != 0) {
//            viewModel.getUserById(userId)
//            viewModel.getUser.observe(this, Observer {
//                setupView(it)
//            })
//        }
//    }
//
//    private fun setupView(data: User) {
//        binding.etUsername.editText?.setText(data.userName)
//        binding.etEmail.editText?.setText(data.email)
//        binding.textView8.text = "User Profile: ${data.userName}"
//
//        val getImage = data.imageProfile
//
//        if (getImage != null) {
//            val imageBytes = Base64.decode(getImage,0)
//            val image = BitmapFactory.decodeByteArray(imageBytes,0,imageBytes.size)
//
//            Glide.with(this)
//                .load(image)
//                .centerCrop()
//                .into(binding.ivImageProfile)
//        }
//    }
//    private  fun logout() {
//        viewModelLogin.setLogout()
//
//        val intent = Intent(this,LoginActivity::class.java)
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//        startActivity(intent)
//        Toast.makeText(this,"Berhasil logout...",Toast.LENGTH_LONG).show()
//    }
    private fun logout() {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Logout")
        dialog.setMessage("Apakah anda yakin ingin logout dari aplikasi?")

        dialog.setCancelable(true)
        dialog.setPositiveButton("YES"){_ , _ ->
            viewModelLogin.setLogout()

            val intent = Intent(this,LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            Toast.makeText(this,"Berhasil logout...",Toast.LENGTH_LONG).show()
        }

        dialog.setNegativeButton("NO"){ dialogInterface, _->
            dialogInterface.dismiss()
        }

        dialog.show()
    }

    private fun inputCheck(username: String, email:String, oldPass: String, newPass: String): Boolean {
        return !(TextUtils.isEmpty(username) || TextUtils.isEmpty(email) ||
                TextUtils.isEmpty(oldPass) || TextUtils.isEmpty(newPass))
    }

    private fun checkingPermission() {
        if (isGranted(
                this,
                android.Manifest.permission.CAMERA,
                arrayOf(
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                REQUEST_CODE_PERMISSION )
        ) {
            chooseImageDialog()
        }
    }

    private fun isGranted(
        activity: Activity,
        permission: String,
        permissions: Array<String>,
        request: Int
    ): Boolean {
        val permissionCheck = ActivityCompat.checkSelfPermission(activity, permission)
        return if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(activity, permissions, request)
            }
            false
        } else {
            true
        }
    }

    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(this)
            .setTitle("permission denied")
            .setMessage("please allow the permission")
            .setPositiveButton("App Setting") { _,_ ->
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts("package", packageName, null)
                intent.data = uri
                startActivity(intent)
            }
            .setNegativeButton("cancle") { dialog,_ -> dialog.cancel()}
            .show()
    }

    private fun chooseImageDialog() {
        AlertDialog.Builder(this)
            .setMessage("Pilih Gambar")
//            .setPositiveButton("Gallery") {_,_ -> openGallery() }
//            .setNegativeButton("Camera") {_,_ -> openCamera() }
            .show()
    }
//
//    private val galleryResult =
//        registerForActivityResult(ActivityResultContracts.GetContent()) { result ->
//            val toBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, Uri.parse(result.toString()))
//            val bitmapToString = bitmapToString(toBitmap)
//            imageProfile = bitmapToString
//
//            Glide.with(this)
//                .load(result)
//                .centerCrop()
//                .into(binding.ivImageProfile)
//        }
//
//    private fun openGallery() {
//        intent.type = "image/*"
//        galleryResult.launch("image/*")
//    }
//
//    private val cameraResult =
//        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
//            if (result.resultCode == Activity.RESULT_OK) {
//                handleCameraImage(result.data)
//            }
//        }
//
//    private fun handleCameraImage(intent: Intent?) {
//        val bitmap = intent?.extras?.get("data") as Bitmap
//        imageProfile = bitmapToString(bitmap)
//        Glide.with(this)
//            .load(bitmap)
//            .centerCrop()
//            .into(binding.ivImageProfile)
//    }
//
//    private fun openCamera() {
//        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        cameraResult.launch(cameraIntent)
//    }
//
//    private fun bitmapToString(bitmap: Bitmap): String {
//        val byteArrayStream = ByteArrayOutputStream()
//        bitmap.compress(Bitmap.CompressFormat.PNG,100, byteArrayStream)
//        Bitmap.createScaledBitmap(bitmap, 150,150,false)
//        val toByteArray = byteArrayStream.toByteArray()
//
//        return Base64.encodeToString(toByteArray,Base64.DEFAULT)
//    }
//
    companion object {
        val REQUEST_CODE_PERMISSION = 99
    }
}

@Composable
fun LogoutAlert(logout: ()->Unit) {

    var openDialog by remember { mutableStateOf(true) }
    if (openDialog) {
        AlertDialog(
            onDismissRequest = { openDialog = false },
            title = { Text(text = "Logout") },
            text = { Text("Apakah anda yakin ingin keluar dari aplikasi?") },
            confirmButton = {
                TextButton(
                    onClick = {
                        openDialog = false
                        logout()
                    }
                ) {
                    Text("Yes")
                }
            },
            dismissButton = {
                TextButton(
                    onClick = {
                        openDialog = false
                    }
                ) {
                    Text("No")
                }
            },
            backgroundColor = colorResource(id = R.color.white),
            contentColor = colorResource(id = R.color.dark_200)
        )
    }
}