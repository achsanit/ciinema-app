package com.example.ciinemaapp.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.ciinemaapp.ui.adapter.GenreListAdapter
import com.example.ciinemaapp.ui.adapter.MovieListAdapter
import com.example.ciinemaapp.databinding.ActivityDetailMovieBinding
import com.example.ciinemaapp.data.remote.model.DetailMovieResponse
import com.example.ciinemaapp.data.remote.model.Genre
import com.example.ciinemaapp.data.remote.model.Result
import com.example.ciinemaapp.ui.viewmodel.DetailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailMovieActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailMovieBinding
    private val viewModel: DetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailMovieBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val movieId = intent.getIntExtra("MOVIE_ID",0)
        getDetailMovie(movieId)

        binding.btnBack.setOnClickListener {
            finish()
        }
    }

    private fun getDetailMovie(movieId: Int) {
        if (movieId != 0) {
            viewModel.getSimilarMovie(movieId)
            viewModel.getDetailMovie(movieId)
            viewModel.detailMovie.observe(this, Observer {
                setupView(it)
            })
            viewModel.genres.observe(this, Observer {
                setupViewGenre(it)
            })
            viewModel.listSimilar.observe(this, Observer {
                setupSimilarMovie(it)
            })
        } else {
            Toast.makeText(this, "Movie id tidak ada", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setupSimilarMovie(data: List<Result>) {
        val recyler = binding.recyclerSimilarMovie
        val adapter = MovieListAdapter(object : MovieListAdapter.OnClickListener{
            override fun onClickItem(data: Result) {
//                Toast.makeText(this@HomeActivity, "${data.id}", Toast.LENGTH_SHORT).show()
//                val intent = Intent(this, DetailMovieActivity::class.java)
//                intent.putExtra("MOVIE_ID", data.id)
//                startActivity(intent)
                getDetailMovie(data.id)
            }
        })
        adapter.submitData(data)
        recyler.adapter = adapter
        recyler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun setupViewGenre(data : List<Genre>) {
        val recyler = binding.recyclerViewGenre
        val adapter = GenreListAdapter()
        adapter.submitData(data)
        recyler.adapter = adapter
        recyler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun setupView(data : DetailMovieResponse) {
        val imageBackdrop = "https://image.tmdb.org/t/p/w500${data.backdropPath}"
        Glide.with(this)
            .load(imageBackdrop)
            .into(binding.ivBackdrop)

        binding.tvTitleDetail.text = data.title
        binding.tvDetailRating.text = data.voteAverage.toString()
        binding.tvTime.text = "${data.runtime.toString()} minutes"
        binding.tvDate.text = data.releaseDate
        binding.tvOverview.text = data.overview
        binding.progressBar3.visibility = View.GONE
    }
}