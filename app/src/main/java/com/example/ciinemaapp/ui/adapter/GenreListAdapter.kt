package com.example.ciinemaapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ciinemaapp.R
import com.example.ciinemaapp.data.remote.model.Genre
import kotlinx.android.synthetic.main.genre_item.view.*

class GenreListAdapter: RecyclerView.Adapter<GenreListAdapter.ViewHolder>() {
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    private var list: List<Genre> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.genre_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = list[position]

        holder.itemView.tvGenre.text = currentItem.name
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun submitData(data: List<Genre>) {
        this.list = data
    }
}