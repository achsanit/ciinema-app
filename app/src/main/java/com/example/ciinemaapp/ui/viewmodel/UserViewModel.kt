package com.example.ciinemaapp.ui.viewmodel

import androidx.lifecycle.*
import com.example.ciinemaapp.data.local.datastore.DataStoreManager
import com.example.ciinemaapp.data.local.model.User
import com.example.ciinemaapp.data.local.repository.UserRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(
    private val userRepo: UserRepo,
    private val pref: DataStoreManager
): ViewModel() {

    private val _getUser = MutableLiveData<User>()
    val getUser: LiveData<User> = _getUser

    fun getUserIdDS(): LiveData<Int> {
        return pref.getUserId().asLiveData()
    }

    fun getUserById(userId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            userRepo.getUserById(userId,_getUser)
        }
    }

    fun updateUser(user: User) {
        viewModelScope.launch(Dispatchers.IO) {
            userRepo.updateUser(user,_getUser)
        }
    }

}