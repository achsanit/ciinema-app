package com.example.ciinemaapp.ui.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import com.example.ciinemaapp.data.local.model.User
import com.example.ciinemaapp.ui.viewmodel.RegisterViewModel
import com.example.ciinemaapp.R
import com.example.ciinemaapp.ui.view.compose.*
import com.example.ciinemaapp.ui.view.compose.component.ButtonPrimary
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterActivity : AppCompatActivity() {
    private val viewModel: RegisterViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent{
            Column(
                modifier = Modifier
                    .background(color = colorResource(id = R.color.dark_700))
                    .fillMaxHeight()
            ) {
                var username by remember { mutableStateOf(TextFieldValue("")) }
                var email by remember { mutableStateOf(TextFieldValue("")) }
                var password by remember { mutableStateOf(TextFieldValue("")) }
                var confirmPassword by remember { mutableStateOf(TextFieldValue("")) }

                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(64.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    headingOne("REGISTER")
                }
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(64.dp, 18.dp, 64.dp, 0.dp)
                ) {
                    textFieldPrimary(
                        label = "Username",
                        textFieldValue = username,
                        onValueChange = { username = it }
                    )

                    textFieldPrimary(
                        label = "Email",
                        textFieldValue = email,
                        onValueChange = { email = it }
                    )

                    passwordTextFieldPrimary(
                        label = "Password",
                        textFieldValue = password,
                        onValueChange = { password = it }
                    )

                    passwordTextFieldPrimary(
                        label = "Confirm Password",
                        textFieldValue = confirmPassword,
                        onValueChange = { confirmPassword = it }
                    )
                }
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(64.dp, 64.dp, 64.dp, 0.dp)
                ) {
                    ButtonPrimary(
                        text = "REGISTER",
                        onClick = {
                            register(username.text,email.text,password.text,confirmPassword.text)
                        }
                    )
                }
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    clickableText("i already have an account", onClick = {
                        startActivity(Intent(this@RegisterActivity, LoginActivity::class.java))
                        finish()
                    })
                }
            }
        }
    }

    private fun register(username: String,email: String,pass: String,confirmPass: String) {

        if (inputCheck(username,email,pass,confirmPass)) {
            if (pass == confirmPass) {
                val newuser = User(0,username,email,pass,null)
                viewModel.userRegister(newuser)

                Toast.makeText(this, "Registrasi berhasil..", Toast.LENGTH_SHORT).show()

                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            } else {
                Toast.makeText(this, "Konfirmasi password salah!!", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "lengkapi isi fields!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun inputCheck(username: String, email:String, password: String, confirmPass: String): Boolean {
        return !(TextUtils.isEmpty(username) || TextUtils.isEmpty(email) ||
                TextUtils.isEmpty(password) || TextUtils.isEmpty(confirmPass))
    }
}