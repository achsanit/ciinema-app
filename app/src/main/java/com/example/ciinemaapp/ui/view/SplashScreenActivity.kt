package com.example.ciinemaapp.ui.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Observer
import com.example.ciinemaapp.databinding.ActivitySplashScreenBinding
import com.example.ciinemaapp.data.local.datastore.DataStoreManager
import com.example.ciinemaapp.ui.viewmodel.LoginViewModel
import com.example.ciinemaapp.R
import com.example.ciinemaapp.ui.view.compose.component.LottieLoad
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashScreenActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashScreenBinding
    private val viewModel: LoginViewModel by viewModels()
    private lateinit var pref: DataStoreManager
    private val SHAREDPREF_FILE = "shared_login"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
//        setContentView(binding.root)
        setContent {
            Column(
                modifier = Modifier
                    .fillMaxHeight()
                    .fillMaxWidth()
                    .background(
                        colorResource(R.color.dark_700)
                    ),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Column(modifier = Modifier.padding(92.dp)) {
                    LottieLoad()
                }
            }
        }

//        pref = DataStoreManager(this)
//        viewModel = ViewModelProvider(this, ViewModelFactory(this,pref))[LoginViewModel::class.java]

        viewModel.getStatusLogin().observe(this, Observer {
            if (it) {
                Handler(Looper.getMainLooper()).postDelayed({
                    val goToHome = Intent(this, HomeActivity::class.java)
                    startActivity(goToHome)
                    finish()
                },3000)
            } else {
                Handler(Looper.getMainLooper()).postDelayed({
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                },3000)
            }
        })

//        val sharedPref: SharedPreferences = this.getSharedPreferences(SHAREDPREF_FILE, Context.MODE_PRIVATE)
//
//        if (sharedPref.contains(LoginActivity.IS_USER_LOGIN)) {
//            Handler(Looper.getMainLooper()).postDelayed({
//                val goToHome = Intent(this, HomeActivity::class.java)
//                startActivity(goToHome)
//                finish()
//            },3000)
//        } else {
//            Handler(Looper.getMainLooper()).postDelayed({
//                val intent = Intent(this, LoginActivity::class.java)
//                startActivity(intent)
//                finish()
//            },3000)
//        }
    }

}

