package com.example.ciinemaapp.ui.viewmodel

import androidx.lifecycle.*
import com.example.ciinemaapp.data.remote.model.DetailMovieResponse
import com.example.ciinemaapp.data.remote.model.Genre
import com.example.ciinemaapp.data.remote.model.Result
import com.example.ciinemaapp.data.remote.repository.MovieRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(private val movieRepo : MovieRepo): ViewModel() {

    private val _detailMovie = MutableLiveData<DetailMovieResponse>()
    val detailMovie: LiveData<DetailMovieResponse> = _detailMovie
    private val _genres = MutableLiveData<List<Genre>>()
    val genres: LiveData<List<Genre>> = _genres
    private val _listSimilar = MutableLiveData<List<Result>>()
    val listSimilar: LiveData<List<Result>> = _listSimilar

    fun getSimilarMovie(movieId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            movieRepo.getSimilarMovie(movieId, _listSimilar)
        }
    }

    fun getDetailMovie(movieId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            movieRepo.getDetailMovie(movieId,_detailMovie,_genres)
        }
    }

}