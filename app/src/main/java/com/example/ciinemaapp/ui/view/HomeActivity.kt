package com.example.ciinemaapp.ui.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ciinemaapp.ui.adapter.MovieListAdapter
import com.example.ciinemaapp.ui.adapter.ViewPagerAdapter
import com.example.ciinemaapp.databinding.ActivityHomeBinding
import com.example.ciinemaapp.data.local.datastore.DataStoreManager
import com.example.ciinemaapp.data.remote.model.Result
import com.example.ciinemaapp.ui.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint

//import com.example.ciinemaapp.ui.viewmodel.ViewModelFactory

@AndroidEntryPoint
class HomeActivity : AppCompatActivity(){
    private lateinit var binding: ActivityHomeBinding
    private val SHAREDPREF_FILE = "shared_login"
    private val viewModel: HomeViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivGoToUserProfile.setOnClickListener {
            startActivity(Intent(this,UserProfileActivity::class.java))
        }

//        viewModel = ViewModelProvider(
//            this,
//            ViewModelFactory(this,DataStoreManager(this))
//        )[HomeViewModel::class.java]

        setupView()

        getListMovie()

        getListTv()

        getNowPlaying()

    }

    private fun setupView() {
//        val sharedPref = this.getSharedPreferences(SHAREDPREF_FILE, Context.MODE_PRIVATE)
//        binding.tvUsername.text = "Hallo ${sharedPref.getString(LoginActivity.KEY_NAME, "")}"
        viewModel.getUserIdDS().observe(this, Observer {
            viewModel.getUserById(it)
        })
        viewModel.getUser.observe(this, Observer {
            binding.tvUsername.text = "Hallo, ${it.userName}"
        })
    }

    private fun getListMovie() {
        viewModel.getPopularMovie()
        viewModel.listMovie.observe(this, Observer {
            showList(it,binding.recyclerMovie)
        })
    }

    private fun getNowPlaying() {
        viewModel.getNowPlayingMovie()
        viewModel.listNowPlaying.observe(this, Observer {
            setupViewPager(it)
        })
    }

    private fun getListTv() {
        viewModel.getTopRatedMovie()
        viewModel.listTopRatedMovie.observe(this, Observer {
            showList(it,binding.recyclerTv)
            binding.progressBar2.visibility = View.GONE
        })
    }

    private fun setupViewPager(data: List<Result>) {
        val viewPager = binding.viewPager2
        val adapter = ViewPagerAdapter(object : MovieListAdapter.OnClickListener{
            override fun onClickItem(data: Result) { goToDetail(data) }
        })
        adapter.submitData(data)
        viewPager.adapter = adapter

        val pagerIndicator = binding.indicator
        pagerIndicator.attachToPager(viewPager)
    }

    private fun showList(data: List<Result>, recyclerView: RecyclerView) {
        val adapter = MovieListAdapter(object : MovieListAdapter.OnClickListener{
            override fun onClickItem(data: Result) { goToDetail(data) }
        })
        adapter.submitData(data)
        val recyler = recyclerView
        recyler.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        recyler.layoutManager = linearLayoutManager
        recyler.adapter = adapter
    }

    private fun goToDetail(data : Result) {
        val intent = Intent(this@HomeActivity, DetailMovieActivity::class.java)
        intent.putExtra("MOVIE_ID", data.id)
        startActivity(intent)
    }
}