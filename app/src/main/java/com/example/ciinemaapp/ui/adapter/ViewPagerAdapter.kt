package com.example.ciinemaapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.ciinemaapp.R
import com.example.ciinemaapp.data.remote.model.Result
import kotlinx.android.synthetic.main.item_viewpager.view.*

class ViewPagerAdapter(private val onItemClick: MovieListAdapter.OnClickListener): RecyclerView.Adapter<ViewPagerAdapter.ViewHolder>() {
    private var list: List<Result> = arrayListOf()

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_viewpager, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentList = list[position]
        val imgUrl = "https://image.tmdb.org/t/p/w500${currentList.backdropPath}"

        Glide.with(holder.itemView.context)
            .load(imgUrl)
            .into(holder.itemView.ivBackdropMovie)

        holder.itemView.tvTitleNowPlaying.text = currentList.title
        holder.itemView.cardNowPlaying.setOnClickListener {
            onItemClick.onClickItem(currentList)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun submitData(data: List<Result>) {
        this.list = data
    }

}