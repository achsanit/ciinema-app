package com.example.ciinemaapp.ui.view.compose.ui.theme

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import com.example.ciinemaapp.R

val Rubik = FontFamily(
    Font(R.font.rubik_medium),
    Font(R.font.rubik_bold, FontWeight.Bold)
)