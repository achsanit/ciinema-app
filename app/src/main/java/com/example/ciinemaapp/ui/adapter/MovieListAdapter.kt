package com.example.ciinemaapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.ciinemaapp.R
import com.example.ciinemaapp.data.remote.model.Result
import kotlinx.android.synthetic.main.movie_item.view.*

class MovieListAdapter(private val onItemClick: OnClickListener): RecyclerView.Adapter<MovieListAdapter.ViewHolder>(){
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val diffCallback = object : DiffUtil.ItemCallback<Result>() {
        override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }

    }

    private val differ = AsyncListDiffer(this,diffCallback)

    fun submitData(data: List<Result>) {
        differ.submitList(data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentList =  differ.currentList[position]
        val poster = "https://image.tmdb.org/t/p/w500${currentList.posterPath}"

        Glide.with(holder.itemView.context)
            .load(poster)
            .into(holder.itemView.ivPoster)
        val rating = ((currentList.voteAverage * 10).toInt()).toDouble()/10
        holder.itemView.tvRating.text = rating.toString()
        holder.itemView.cardRecyclerItem.setOnClickListener {
            onItemClick.onClickItem(currentList)
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    interface OnClickListener {
        fun onClickItem(data: Result)
    }
}