package com.example.ciinemaapp.ui.view.compose.component

import android.widget.Toast
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import com.example.ciinemaapp.ui.view.compose.normalText
import com.example.ciinemaapp.R

@Composable
fun ButtonPrimary(text :String, onClick: ()->Unit) {
    Column(modifier = Modifier.padding(vertical = 8.dp)) {
        Button(onClick = { onClick() },
            modifier = Modifier
                .fillMaxWidth()
                .height(48.dp),
            shape = RoundedCornerShape(8.dp)
        ) {
            normalText(text)
        }
    }
}

@Composable
fun ButtonSecondary(text: String, onClick: () -> Unit) {
    Column(modifier = Modifier.padding(vertical = 8.dp)) {
        OutlinedButton(onClick = { onClick() },
            modifier = Modifier
                .fillMaxWidth()
                .height(48.dp),
            shape = RoundedCornerShape(8.dp),
            border = BorderStroke(2.dp, color = colorResource(id = R.color.blue_acent)),
            colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.dark_700))
        ) {
            normalText(text)
        }
    }
}

@Composable
fun ButtonPrimaryCompose(text :String, onClick: @Composable ()->Unit) {
    Button(
        onClick =  { },
        modifier = Modifier
            .fillMaxWidth()
            .height(48.dp),
        shape = RoundedCornerShape(8.dp)
    ) {
        normalText(text)
    }
}