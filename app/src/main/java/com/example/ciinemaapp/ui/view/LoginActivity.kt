package com.example.ciinemaapp.ui.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.lifecycle.Observer
import com.example.ciinemaapp.R
import com.example.ciinemaapp.ui.viewmodel.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.runtime.*
import com.example.ciinemaapp.ui.view.compose.*
import com.example.ciinemaapp.ui.view.compose.component.ButtonPrimary

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {
    private val viewModel: LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Column(
                modifier = Modifier
                    .background(color = colorResource(id = R.color.dark_700))
                    .fillMaxHeight(),
            ) {
                var username by remember { mutableStateOf(TextFieldValue("")) }
                var password by remember { mutableStateOf(TextFieldValue("")) }
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(64.dp)
                ) {
                    headingOne("LOGIN")
                    normalText("Please Login, to see list of \npopular movies")
                }
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(64.dp, 18.dp, 64.dp, 0.dp)
                ) {

                    textFieldPrimary(
                        label = "Username",
                        textFieldValue = username,
                        onValueChange = { username = it }
                    )

                    passwordTextFieldPrimary(
                        label = "Password",
                        textFieldValue = password,
                        onValueChange = { password = it }
                    )
                }
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(64.dp, 64.dp, 64.dp, 0.dp)
                ) {
                    ButtonPrimary(
                        "LOGIN",
                        onClick = {
                            login(username.text,password.text)
                        }
                    )
                }
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    clickableText("i don't have an account", onClick = {
                        startActivity(Intent(this@LoginActivity, RegisterActivity::class.java)) })
                }
            }
        }
    }

    private fun login(username: String, password: String) {

        if (inputCheck(username,password)) {
            viewModel.getUserLogin(username,password)
            viewModel.user.observe(this, Observer {
                if (it == null) {
                    Toast.makeText(this, "Sorry, user not found", Toast.LENGTH_SHORT).show()
                } else {

                    viewModel.setUserId(it.userId)
                    viewModel.setLogin()

                    val intent = Intent(this,HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                    Toast.makeText(this, "Login successful", Toast.LENGTH_SHORT).show()
                }
            })

        } else {
            Toast.makeText(this, "Opps.. field is empty", Toast.LENGTH_SHORT).show()
        }
    }

    private fun inputCheck(username: String, password: String): Boolean {
        return !(TextUtils.isEmpty(username) || TextUtils.isEmpty(password))
    }
}