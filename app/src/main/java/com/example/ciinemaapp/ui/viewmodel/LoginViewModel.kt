package com.example.ciinemaapp.ui.viewmodel

import androidx.lifecycle.*
import com.example.ciinemaapp.data.local.datastore.DataStoreManager
import com.example.ciinemaapp.data.local.model.User
import com.example.ciinemaapp.data.local.repository.UserRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val userRepo: UserRepo,
    private val pref: DataStoreManager
): ViewModel() {

    private val _user = MutableLiveData<User>()
    val user: LiveData<User> get() = _user

    fun getUserLogin(username: String, pass: String) {
        viewModelScope.launch(Dispatchers.IO) {
            userRepo.getUserLogin(username, pass ,_user)
        }
    }

    fun setLogin() {
        viewModelScope.launch {
            pref.setLoginStatus()
        }
    }

    fun setLogout() {
        viewModelScope.launch {
            pref.setLogoutStatus()
        }
    }

    fun setUserId(userId: Int) {
        viewModelScope.launch {
            pref.setUserId(userId)
        }
    }

    fun getStatusLogin(): LiveData<Boolean> {
        return pref.statusLogin().asLiveData()
    }
}