package com.example.ciinemaapp.ui.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import com.example.ciinemaapp.data.local.datastore.DataStoreManager
import com.example.ciinemaapp.data.remote.model.Result
import com.example.ciinemaapp.data.local.model.MyDb
import com.example.ciinemaapp.data.local.model.User
import com.example.ciinemaapp.data.remote.repository.MovieRepo
import com.example.ciinemaapp.data.local.repository.UserRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val userRepo: UserRepo,
    private val movieRepo: MovieRepo,
    private val pref: DataStoreManager
): ViewModel() {

    private val _getUser = MutableLiveData<User>()
    val getUser: LiveData<User> = _getUser

    //list movie
    private val _listMovie = MutableLiveData<List<Result>>()
    val listMovie: LiveData<List<Result>> get() = _listMovie

    //list top rated
    private val _listTopRatedMovie = MutableLiveData<List<Result>>()
    val listTopRatedMovie: LiveData<List<Result>> get() = _listTopRatedMovie

    //now playing
    private val _listNowPlay = MutableLiveData<List<Result>>()
    val listNowPlaying: LiveData<List<Result>> = _listNowPlay

    fun getUserIdDS(): LiveData<Int> {
        return pref.getUserId().asLiveData()
    }

    fun getUserById(userId: Int) {
        Log.d("userid", "$userId")
        viewModelScope.launch(Dispatchers.IO) {
            userRepo.getUserById(userId,_getUser)
        }
    }

    fun getNowPlayingMovie() {
        viewModelScope.launch(Dispatchers.IO) {
            movieRepo.getNowPlaying(_listNowPlay)
        }
    }

    fun getPopularMovie(){
        viewModelScope.launch (Dispatchers.IO){
            movieRepo.getPopularMovie(_listMovie)
        }
    }

    fun getTopRatedMovie(){
        viewModelScope.launch (Dispatchers.IO){
            movieRepo.getTopRatedMovie(_listTopRatedMovie)
        }
    }

}