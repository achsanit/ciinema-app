package com.example.ciinemaapp.ui.view.compose

import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.ciinemaapp.R
import com.example.ciinemaapp.ui.view.compose.ui.theme.Rubik
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.integerArrayResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun headingOne(text: String) {
    Column(
        modifier = Modifier.padding(0.dp,12.dp)
    ) {
        Text(
            text = text,
            style = TextStyle(
                fontSize = 32.sp,
                fontFamily = Rubik,
                fontWeight = FontWeight.Bold,
                color = colorResource(R.color.white)
            )
        )
    }
}

@Composable
fun normalText(text:String, style: TextStyle? = null) {
    Text(
        text = text,
        style = style ?: TextStyle(
            fontSize = 14.sp,
            color = colorResource(id = R.color.line_dark),
            fontFamily = Rubik
        )
    )
}

@Composable
fun  textFieldPrimary(label: String, textFieldValue: TextFieldValue, onValueChange: (TextFieldValue) -> Unit) {
    TextField(
        value = textFieldValue,
        modifier = Modifier
            .fillMaxWidth()
            .padding(0.dp, 8.dp),
        onValueChange = onValueChange,
        label = { Text(text = label) },
        colors = TextFieldDefaults.textFieldColors(
            textColor = colorResource(id = R.color.dark_700),
            backgroundColor = colorResource(id = R.color.white)
        )
    )
}

@Composable
fun  passwordTextFieldPrimary(label: String, textFieldValue: TextFieldValue, onValueChange: (TextFieldValue) -> Unit) {
    var passwordVisible by remember { mutableStateOf(false) }

    TextField(
        value = textFieldValue,
        modifier = Modifier
            .fillMaxWidth()
            .padding(0.dp, 8.dp),
        onValueChange = onValueChange,
        label = { Text(text = label) },
        visualTransformation = if (passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
        colors = TextFieldDefaults.textFieldColors(
            textColor = colorResource(id = R.color.dark_700),
            backgroundColor = colorResource(id = R.color.white)),
        trailingIcon = {
            val image = if (passwordVisible)
                Icons.Filled.Visibility
            else Icons.Filled.VisibilityOff

            // Please provide localized description for accessibility services
            val description = if (passwordVisible) "Hide password" else "Show password"

            IconButton(onClick = {passwordVisible = !passwordVisible}){
                Icon(imageVector  = image, description)
            }
        }
    )
}

@Composable
fun clickableText(displayText: String, onClick : ()->Unit) {
    var enabled by remember { mutableStateOf(true)}

    ClickableText(
        text = AnnotatedString(text =displayText),
        onClick = {
            if (enabled) {
                enabled = false
            }
            onClick() },
        style = TextStyle(
            fontSize = 14.sp,
            color = colorResource(id = R.color.line_dark),
            fontFamily = Rubik
        )
    )
}

