package com.example.ciinemaapp.data.remote.repository

import com.example.ciinemaapp.BuildConfig
import com.example.ciinemaapp.data.remote.model.DetailMovieResponse
import com.example.ciinemaapp.data.remote.model.MovieResponse
import com.example.ciinemaapp.data.remote.services.ApiHelper
import com.example.ciinemaapp.data.remote.services.ApiService
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before

import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApiHelperTest {

    lateinit var apiHelper: ApiHelper

    @Mock
    lateinit var apiService: ApiService
    @Mock
    lateinit var movieResponse: Call<MovieResponse>
    @Mock
    lateinit var detailMovieResponse: Call<DetailMovieResponse>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        apiHelper = ApiHelper(apiService)
    }

    @Test
    fun getNowPlaying() {
        runBlocking {
            Mockito.`when`(apiService.getNowPlaying(BuildConfig.API_KEY,1,"en-US"))
                .thenReturn(movieResponse)

            val result = apiHelper.getNowPlaying(BuildConfig.API_KEY,1,"en-US")
            assertEquals(movieResponse,result)
        }
    }

    @Test
    fun getDetailMovie() {
        runBlocking {
            Mockito.`when`(apiService.getDetailMovie(1,BuildConfig.API_KEY,"en-US"))
                .thenReturn(detailMovieResponse)

            val result = apiHelper.getDetailMovie(1,BuildConfig.API_KEY,"en-US")
            assertEquals(detailMovieResponse,result)
        }
    }

    @Test
    fun getPopularMovie() {
        runBlocking {
            Mockito.`when`(apiService.getPopularMovie(BuildConfig.API_KEY,1,"en-US"))
                .thenReturn(movieResponse)

            val result = apiHelper.getPopularMovie(BuildConfig.API_KEY,1,"en-US")
            assertEquals(movieResponse,result)
        }
    }

    @Test
    fun getSimilarMovie() {
        runBlocking {
            Mockito.`when`(apiService.getSimilarMovie(1,BuildConfig.API_KEY,"en-US"))
                .thenReturn(movieResponse)

            val result = apiHelper.getSimilarMovie(1,BuildConfig.API_KEY,"en-US")
            assertEquals(movieResponse,result)
        }
    }

    @Test
    fun getTopRatedMovie() {
        runBlocking {
            Mockito.`when`(apiService.getTopRatedMovie(BuildConfig.API_KEY,1, "en-US"))
                .thenReturn(movieResponse)

            val result = apiHelper.getTopRatedMovie(BuildConfig.API_KEY,1,"en-US")
            assertEquals(movieResponse,result)
        }
    }

}