package com.example.ciinemaapp.ui.viewmodel

//import android.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.arch.core.executor.DefaultTaskExecutor
import androidx.arch.core.executor.TaskExecutor
import androidx.lifecycle.MutableLiveData
import com.example.ciinemaapp.data.remote.model.DetailMovieResponse
import com.example.ciinemaapp.data.remote.model.Genre
import com.example.ciinemaapp.data.remote.repository.MovieRepo
import com.example.ciinemaapp.data.remote.services.ApiHelper
import com.example.ciinemaapp.data.remote.services.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import okhttp3.internal.notifyAll
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class DetailViewModelTest {

//    @get:Rule
//    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()

    lateinit var detailViewModel: DetailViewModel
    lateinit var movieRepo: MovieRepo
    lateinit var apiHelper: ApiHelper

    var detailMovie = MutableLiveData<DetailMovieResponse>()
    var genre = MutableLiveData<List<Genre>>()

    @Mock
    lateinit var apiService: ApiService
    @Mock
    lateinit var detailMovieResponse: DetailMovieResponse

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        apiHelper = ApiHelper(apiService)
        movieRepo = MovieRepo(apiHelper)
        detailViewModel = DetailViewModel(movieRepo)
    }

    fun detailMovie() {
        detailMovie.postValue(detailMovieResponse)
    }

    @Test
    fun getDetailMovie() {
        runBlocking {
            Mockito.`when`(movieRepo.getDetailMovie(1,detailMovie,genre))
                .thenReturn(detailMovie())

            detailViewModel.getDetailMovie(1)
            val result = detailViewModel.detailMovie.getOrAwaitValue()
            assertEquals(result,detailMovie)
        }
    }

}